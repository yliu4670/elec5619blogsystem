# README #

Welcome to our Spring blog system, here is the introduction of how to run our program.

### How to create Spring MVC project###

We built our project by using IntelliJ IDEA, before building a Spring MVC project, you should do the following steps:

* set up the Tomcat environment
* use Maven to import the jar package related to SpringMVC like spring-core, spring-context
* configure your web.xml to add a mvc-dispatcher to dispatch the url request
* add a mvc-dispatcher-servlet.xml and add some configuration like the location of controller
### How to run our project ###

* build a database called springdemo in mysql, source the blog.sql file into the database.
* in IDEA, click "Edit Configuration" to configure the Tomcat
* Select Tomcat server as the server
* confiure the Tomcat


### How to test###

* run the Controller test


### Our team member ###
YIMING LIU

ZHELI LIU

YIDING SHI

XINYU ZHENG