package com.elec5619.test;

import  com.elec5619.controller.MainController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.ServletContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml"})

public class MainControllerTest {

    @Autowired
    MainController mainController;
    @Autowired
    ServletContext context;
    private MockMvc mockMvc;
    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    public void about() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/about").sessionAttr("userId",2))
                .andExpect(model().attributeExists("id"))
                .andExpect(view().name("user/aboutUser"))
                .andExpect(forwardedUrl("user/aboutUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void adminAbout() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/adminAbout"))
                .andExpect(view().name("user/adminAboutUser"))
                .andExpect(forwardedUrl("user/adminAboutUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void getUser() throws Exception {
        ResultActions resultActions=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/adminViewUser"))
                .andExpect(model().attributeExists("adminList"))
                .andExpect(view().name("user/adminViewUser"))
                .andExpect(forwardedUrl("user/adminViewUser"))
                .andDo(print());
    }

    @org.junit.Test
    public void getAdminBlog() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/adminViewBlog"))
                .andExpect(model().attributeExists("blogList"))
                .andExpect(view().name("user/adminViewBlog"))
                .andExpect(forwardedUrl("user/adminViewBlog"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    /**
     * perform：执行一个RequestBuilder请求，会自动执行SpringMVC的流程并映射到相应的控制器执行处理；
     * get:声明发送一个get请求的方法。
     * andExpect：添加ResultMatcher验证规则，验证控制器执行完成后结果是否正确（对返回的数据进行的判断）；
     * andDo：添加ResultHandler结果处理器，比如调试时打印结果到控制台（对返回的数据进行的判断）；
     * andReturn：最后返回相应的MvcResult；然后进行自定义验证/进行下一步的异步处理（对返回的数据进行的判断）
     **/

    @Test
    public void getUsers() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/commonViewUser").sessionAttr("userId",2))
                .andExpect(model().attributeExists("adminList"))
                .andExpect(model().attributeExists("id"))
                .andExpect(view().name("user/commonViewUser"))
                .andExpect(forwardedUrl("user/commonViewUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void showUserDetail() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/admin/users/show/2"))
                .andExpect(model().attributeExists("blogList"))
                .andExpect(view().name("user/adminAboutUser"))
                .andExpect(forwardedUrl("user/adminAboutUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void showUserDetails() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/common/users/show/2"))
                .andExpect(model().attributeExists("blogList"))
                .andExpect(view().name("user/aboutUser"))
                .andExpect(forwardedUrl("user/aboutUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void showAdminBlog() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/admin/show/2"))
                .andExpect(model().attributeExists("BlogComment"))
                .andExpect(view().name("user/adminBlogDetail"))
                .andExpect(forwardedUrl("user/adminBlogDetail"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void updateUser() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/admin/users/update/2").sessionAttr("userId",2))
                .andExpect(model().attributeExists("id"))
                .andExpect(view().name("user/updateUser"))
                .andExpect(forwardedUrl("user/updateUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void updateUserPost() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .post("/admin/users/updateP").contentType(MediaType.APPLICATION_JSON)
                .param("nickname","Tom")
                .param("firstName","Tom")
                .param("lastName","Jerry")
                .param("password","password")
                .param("id","2"))
                .andExpect(view().name("redirect:/admin/blogs"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void adminUpdateUser() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .get("/admin/update/2"))
                .andExpect(view().name("user/adminUpdateUser"))
                .andExpect(forwardedUrl("user/adminUpdateUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }

    @org.junit.Test
    public void adminUpdateUserPost() throws Exception {
        String response=this.mockMvc.perform(MockMvcRequestBuilders
                .post("/admin/updateP").contentType(MediaType.APPLICATION_JSON)
                .param("nickname","Tom")
                .param("firstName","Tom")
                .param("lastName","Jerry")
                .param("password","password")
                .param("id","2"))
                .andExpect(view().name("redirect:/adminViewUser"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(response);
    }
}